package com.example.login_activity_taller.ui.login;

import androidx.annotation.NonNull;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.login_activity_taller.ProfileActivity;
import com.example.login_activity_taller.R;
//import com.example.login_activity_taller.ui.login.LoginViewModel;
//import com.example.login_activity_taller.ui.login.LoginViewModelFactory;
import com.example.login_activity_taller.databinding.ActivityLoginBinding;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Date;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG =  "Login Activity";
//    private TextView loginEstado;

    // logout
    private Button Logout;

    //Firebase
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    // Google
    private SignInButton mSignInGoogleButton;
    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInOptions gso;
    private static final int RC_SIGN_IN = 9001;

//    private LoginViewModel loginViewModel;
    private ActivityLoginBinding binding;

//    private LoginViewModel loginViewModel;
//    private ActivityLoginBinding binding;

    // Cloud firestore
    FirebaseFirestore db = FirebaseFirestore.getInstance();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

//        loginEstado = (TextView) findViewById(R.id.loginestado);

        // Firebase
        mAuth = FirebaseAuth.getInstance();



        // Intanciar la clase modelo de datos
//        LoggedInUserView dataStore = new LoggedInUserView("acti", "act",
//                "cours", "IRA", 0.5F, "locat", "time",
//                "PQKDSKCVNCHYD");

        // test
//        System.out.println("INGRESANDO A DATASOTRE");
//        System.out.println(dataStore.getDisplayName());


        //Google


        mSignInGoogleButton = (SignInButton) findViewById(R.id.loginButton_Google);

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener(){
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult){
                        Toast.makeText(getApplicationContext(), "Error al conectarse a Google", Toast.LENGTH_SHORT).show();
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        mSignInGoogleButton.setOnClickListener((View.OnClickListener) (view) -> {
            signIn();
        });


        // Donde empieza el AuthStateListener para escuchar si el usuario
        // se registro o no
        mAuthListener = new FirebaseAuth.AuthStateListener(){
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                // Get signIn user
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null){
//                    checkUserExists();
//                    user.getUid(); // extraer informacion del usuario
                    System.out.println("INGRESANDO A ONAUTHSTATECHANGED");
                    System.out.println(user.getUid().toString());
                    System.out.println(user.getEmail().toString());
                    String email = user.getEmail(); //TODO: redirigir a nuevo activity
                    passingData(user);
//                    loginEstado.setText("Logueado con Google " + email);
                    Log.d(TAG,"onAuthStateChanged:signed_in" + user.getUid());
                } else{
//                    loginEstado.setText("Usuario no logueado ");
                    Log.d(TAG,"onAuthStateCahnged:signed_out");
                }
            }
        };

        // logout
        //- LLEVADO A PROFILE_ACTIVITY
        /*
        Logout = (Button) findViewById(R.id.buttonlogout);

        Logout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                FirebaseAuth.getInstance().signOut();
            }
        });

         */
    }

    public void passingData(FirebaseUser user) {
        System.out.println("Ingresando al Firestore");
        Date date = new Date();

        System.out.println("FECHA --------------");;
        System.out.println(date.toString());

        //DONE: TIMESTAMP
        LoginUserData userData = new LoginUserData("PRACT", "SISTEMAS DIGITALES", "TRISTEZA",
                37.2F, "[100º N, 100º E]", date.toString(), user.getUid().toString());

        userData.dataUserPrint();

        db.collection("student_context").document().set(userData);
        db.collection("student_context").add(userData).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                Log.d("AGREGADO", "DocumentSnapshot added with ID: " + documentReference.getId());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.w("FALLO", "Error adding document", e);
            }
        });
    }

//    private void checkUserExists() {
//        // ver si no es null. check if db exists
//        mDatabase.child(userid).addValueEventListener(new ValueEventListener(){
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot){
//                if (dataSnapshot.exists()) Log.i("Existe", "Existe");
//                else Log.i("No existe", "no existe");
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError){
//                System.out.println("The read failed: " + databaseError.getCode());
//            }
//        });
//    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);;
        Toast.makeText(this, "Login done", Toast.LENGTH_SHORT).show();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct){
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        // loginButton_fa.setVisibility(View.GONE)
        mSignInGoogleButton.setVisibility(View.GONE);

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>(){
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task){
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
                        // loginButton_fa.setVisibility(View.VISIBLE)
                        mSignInGoogleButton.setVisibility(View.VISIBLE);
                        // If sign in fails, display a meesage to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user ca ben handled in the listener
                        if(task.isSuccessful()){
                            Toast.makeText(LoginActivity.this, "Login successfull", Toast.LENGTH_SHORT).show();
                            toProfile();
                        }
                        if(!task.isSuccessful()){
                            Log.v(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(getApplicationContext(), "Authentication failed.", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        // mcallbackManager.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN){
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()){
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else{
                // Google Sign In failed, update UI appropriately
            }
        }
    }

    private void toProfile(){
        Intent intent = new Intent(LoginActivity.this, ProfileActivity.class);
        intent.setFlags(intent.FLAG_ACTIVITY_NEW_TASK | intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onStart(){
        super.onStart();
        if(mAuthListener != null){
            FirebaseAuth.getInstance().signOut();
        }
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mAuthListener != null){
            mAuth.removeAuthStateListener(mAuthListener);
        }
        mAuth.removeAuthStateListener(mAuthListener);
    }
}
